# NumericcOrg

Code and data for numericc org open source data set

* csv dir contains csv files for commit and issues
* sql dir contains sql files to create tables
* dump contains database dump

Steps to build:

One of two methods a), b) - a) is the only viable one right now as all files are not in place for b)

a) automatically, all at once

These notes are terse and assume you know how to pg_restore
from a pg dump file.

The file nupg.sql at dump/pg contains pg_dump sql file approx 50MB 

You will need to create a Postgres database and then use pg_restore on the nupg.sql file 
which will create tables, import all the data into them.  
If you use the pg dump file to do a pg_restore you dont have to do anything else and don't need the csv or sql files. 


b) manually, step-by-step

* Create a PG database
* Create tables using SQL from sql dir
* Use Postgres COPY command to import CSV files into tables


License: 

All data released under Creative Commons Attribution-NonCommercial-ShareAlike (CC BY-NC-SA)

The data restrictions will be loosened up after more data cleaning. This is very preliminary and a proof of concept

All code released under Apache 2.0 license


Slack channel:   

https://join.slack.com/t/numericcorg/shared_invite/enQtNzkzNjc0MzY4NDY3LTlhMmRhMDlmYzJmYjcwMzAzODFjY2NlNmFkOGU4OTcyZGYwNGM1ZjgxMTk0NzE0YTMzZDFhN2I1NGFkNGYxNTg
