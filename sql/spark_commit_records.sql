CREATE TABLE "SPARK_commit_records" (
	cid VARCHAR , 
	message VARCHAR , 
	author_name VARCHAR, 
	author_email VARCHAR, 
	committer_name VARCHAR, 
	committer_email VARCHAR, 
	time DECIMAL, 
	time_offset DECIMAL, 
	tree_id VARCHAR, 
	message_encoding VARCHAR
);
